![banerGit](https://user-images.githubusercontent.com/16636086/106938115-ded34680-671e-11eb-8de4-35fd6d00868a.png)
# Proyectos - FullStack Java Angular SQL

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación | Proyecto | Versión |
| --- | :---:  | :---:  | :---:  | :---: | :---:  |
| David B. | dev1, Backup | Dev | 02/03/2021 |  |   |   |
| Xavi B. | dev2, Backup | Dev | 02/03/2021 |  |   |   |

#### 2. Description
```
Este repositorio contiene una version el juego MasterMind.

```
#### 3. Link a un demo con el proyecto desplegado: https://github.com/
```
* Nombre de la App: [GITTT] (https://github.com/)
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install
```
Java - jdk-8
Apache Maven - http://maven.apache.org/download.cgi
JUnit
IDE - Eclipse Enterprise
```
###### Command line 
```

```

#### 5. Screenshot imagen que indique cómo debe verse el proyecto.

    
----

### End
