package com.team3.MasterMind.controller;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.team3.MasterMind.AppMain.MasterMindApp;
import com.team3.MasterMind.model.dto.Colores;
import com.team3.MasterMind.view.JFrame.VentanaJuego;
import com.team3.MasterMind.view.JFrame.VentanaNivel;
import com.team3.MasterMind.view.JFrame.VentanaOpciones;
import com.team3.MasterMind.view.JPanel.PanelAvanzado;
import com.team3.MasterMind.view.JPanel.PanelMedio;
import com.team3.MasterMind.view.JPanel.PanelPrincipiante;

public class JuegoController {

	// private ProyectoServ proyectoServ;
	// private ProyectoDao proyectoDao;
	private VentanaJuego miVentanaJuego;
	private VentanaNivel miVentanaNivel;
	private VentanaOpciones miVentanaOpciones;

	private PanelPrincipiante miPanelPrincipiante;
	private PanelMedio miPanelMedio;
	private PanelAvanzado miPanelAvanzado;
	
	private Colores colores;

	// Metodos getter Setters de vistas
	public VentanaJuego getMiVentanaJuego() {
		return miVentanaJuego;
	}

	public void setMiVentanaJuego(VentanaJuego miVentanaJuego) {
		this.miVentanaJuego = miVentanaJuego;
	}

	public VentanaNivel getMiVentanaNivel() {
		return miVentanaNivel;
	}

	public void setMiVentanaNivel(VentanaNivel miVentanaNivel) {
		this.miVentanaNivel = miVentanaNivel;
	}

	public VentanaOpciones getMiVentanaOpciones() {
		return miVentanaOpciones;
	}

	public void setMiVentanaOpciones(VentanaOpciones miVentanaOpciones) {
		this.miVentanaOpciones = miVentanaOpciones;
	}

	// Hace visible las vistas de Registro, Consulta y Listar
	public void mostrarVentanaJuego() {
		miVentanaJuego.setVisible(true);
	}

	public void mostrarVentanaOpciones() {
		miVentanaOpciones.setVisible(true);
	}

	public void mostrarVentanaNivel() {
		miVentanaNivel.setVisible(true);
	}

	// Set Principiante / Medio / Avanzado / Limpiar
	public void setPrincipiante() {
		if (MasterMindApp.debug)
			System.out.println("Set Principiante");
		miPanelPrincipiante = new PanelPrincipiante(colores);
		miPanelPrincipiante.setCordinador(this);
		miVentanaJuego.setPanelPrincipiante(miPanelPrincipiante);
	}

	public void setMedio() {
		if (MasterMindApp.debug)
			System.out.println("Set Medio");
		miPanelMedio = new PanelMedio(colores);
		miPanelMedio.setCordinador(this);
		miVentanaJuego.setPanelMedio(miPanelMedio);
	}

	public void setAvanzado() {
		if (MasterMindApp.debug)
			System.out.println("Set Avanzado");
		miPanelAvanzado = new PanelAvanzado(colores);
		miPanelAvanzado.setCordinador(this);
		miVentanaJuego.setPanelAvanzado(miPanelAvanzado);
	}

	// Activar opciones tras primera partida
	public void setHaJugadoTrue() {
		if (MasterMindApp.debug)
			System.out.println("Opciones Desbloqueadas");
		miVentanaJuego.setHaJugado(true);
		miVentanaJuego.activarOpciones();
	}

	// Colores
	public void setColores(Colores colores) {
		this.colores = colores;
	}
	
	public Colores getColores() {
		return colores;
	}
	
	public void editColores(Color c1, Color c2, Color c3, Color c4, Color c5, Color c6) {
		colores.setC1(c1);
		colores.setC2(c2);
		colores.setC3(c3);
		colores.setC4(c4);
		colores.setC5(c5);
		colores.setC6(c6);
	}
	

	// Llamadas a los metodos CRUD de la capa service para validar los datos de las
	// vistas

	/*
	 * public void registrarProyecto(Proyecto miProyecto) {
	 * proyectoServ.validarRegistro(miProyecto); }
	 * 
	 * public Proyecto buscarProyecto(String codigoProyecto) { return
	 * proyectoServ.validarConsulta(codigoProyecto); }
	 * 
	 * public void modificarProyecto(Proyecto miProyecto) {
	 * proyectoServ.validarModificacion(miProyecto); }
	 * 
	 * public void eliminarProyecto(String codigo) {
	 * proyectoServ.validarEliminacion(codigo); }
	 * 
	 * public ArrayList<Proyecto> listarProyecto() { ProyectoDao proyectoDao = new
	 * ProyectoDao(); return proyectoDao.listarProyecto(); }
	 */

}
