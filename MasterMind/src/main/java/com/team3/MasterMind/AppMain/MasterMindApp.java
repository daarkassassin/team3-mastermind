package com.team3.MasterMind.AppMain;

import java.awt.Color;
import java.util.ArrayList;

import com.team3.MasterMind.controller.JuegoController;
import com.team3.MasterMind.model.dto.Colores;
import com.team3.MasterMind.view.JFrame.VentanaJuego;
import com.team3.MasterMind.view.JFrame.VentanaNivel;
import com.team3.MasterMind.view.JFrame.VentanaOpciones;

public class MasterMindApp {

	public static boolean debug;
	public static final double VERSION = 0.2;
	VentanaJuego miVentanaJuego;
	VentanaNivel miVentanaNivel;
	VentanaOpciones miVentanaOpciones;
	JuegoController juegoController;
	Colores colores;

	public static void main(String[] args) {
		MasterMindApp miMasterMindApp = new MasterMindApp();
		miMasterMindApp.setDebug(false);
		miMasterMindApp.iniciar();
		
		
	}

	public void iniciar() {
		/* Se instancian las clases */
		miVentanaJuego = new VentanaJuego();
		miVentanaNivel = new VentanaNivel();
		miVentanaOpciones = new VentanaOpciones();
		juegoController = new JuegoController();
		colores = new Colores();

		/* Se establecen las relaciones entre clases */
		juegoController.setMiVentanaJuego(miVentanaJuego);
		juegoController.setMiVentanaNivel(miVentanaNivel);
		juegoController.setMiVentanaOpciones(miVentanaOpciones);
		juegoController.setColores(colores);
		
		/* Se establecen relaciones con la clase coordinador */
		miVentanaJuego.setCordinador(juegoController);
		miVentanaNivel.setCordinador(juegoController);
		miVentanaOpciones.setCordinador(juegoController);
		
		
		/* Se hace visible la ventana principal */
		System.out.println(colores);
		juegoController.setPrincipiante(); //Inicializamos la primera partida
		juegoController.mostrarVentanaJuego(); //Mostramos la ventana de juego
		
		
		
		

	}

	// SETDEBUG
	public void setDebug(boolean flag) {
		debug = flag;
	}
}
