package com.team3.MasterMind.view.JPanel;

import javax.swing.*;

import com.team3.MasterMind.controller.JuegoController;
import com.team3.MasterMind.model.dto.Colores;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class PanelPrincipiante extends JPanel {
	private static final long serialVersionUID = 1L;
	JuegoController juegoController;
	public JPanel contentPane;

	// Botones ciclo colores 1
	int btn1_1_color, btn2_1_color, btn3_1_color, btn4_1_color;
	// Botones ciclo colores 2
	int btn1_2_color, btn2_2_color, btn3_2_color, btn4_2_color;
	// Botones ciclo colores 3
	int btn1_3_color, btn2_3_color, btn3_3_color, btn4_3_color;
	// Botones ciclo colores 4
	int btn1_4_color, btn2_4_color, btn3_4_color, btn4_4_color;
	// Botones ciclo colores 5
	int btn1_5_color, btn2_5_color, btn3_5_color, btn4_5_color;
	// Botones ciclo colores 6
	int btn1_6_color, btn2_6_color, btn3_6_color, btn4_6_color;
	// Botones ciclo colores 7
	int btn1_7_color, btn2_7_color, btn3_7_color, btn4_7_color;
	// Botones ciclo colores 8
	int btn1_8_color, btn2_8_color, btn3_8_color, btn4_8_color;
	// Botones ciclo colores 9
	int btn1_9_color, btn2_9_color, btn3_9_color, btn4_9_color;
	// Botones ciclo colores 10
	int btn1_10_color, btn2_10_color, btn3_10_color, btn4_10_color;

	public PanelPrincipiante(Colores colores) {
		
		// PANEL
		contentPane = new JPanel();
		contentPane.setLayout(null);
		
		// Array colores
		ArrayList<Color> coloresDisponibles = new ArrayList<Color>();
		
		// Asignar colores
		coloresDisponibles.add(colores.getC1());
		coloresDisponibles.add(colores.getC2());
		coloresDisponibles.add(colores.getC3());
		coloresDisponibles.add(colores.getC4());

		// Colores Disponibles

		JLabel lblColoresDisponibles = new JLabel("Colores Disponibles");
		lblColoresDisponibles.setBounds(25, 30, 150, 14);
		contentPane.add(lblColoresDisponibles);

		JButton btn1_0 = new JButton("");
		btn1_0.setEnabled(false);
		btn1_0.setBackground(coloresDisponibles.get(0));
		btn1_0.setBounds(25, 50, 30, 30);
		contentPane.add(btn1_0);

		JButton btn2_0 = new JButton("");
		btn2_0.setEnabled(false);
		btn2_0.setBackground(coloresDisponibles.get(1));
		btn2_0.setBounds(65, 50, 30, 30);
		contentPane.add(btn2_0);

		JButton btn3_0 = new JButton("");
		btn3_0.setEnabled(false);
		btn3_0.setBackground(coloresDisponibles.get(2));
		btn3_0.setBounds(105, 50, 30, 30);
		contentPane.add(btn3_0);

		JButton btn4_0 = new JButton("");
		btn4_0.setEnabled(false);
		btn4_0.setBackground(coloresDisponibles.get(3));
		btn4_0.setBounds(145, 50, 30, 30);
		contentPane.add(btn4_0);

		// Combinacion Secreta

		JLabel lblCombinacionSecreta = new JLabel("Combinacion Secreta");
		lblCombinacionSecreta.setBounds(305, 30, 150, 14);
		contentPane.add(lblCombinacionSecreta);

		JButton btnColorSecreto_4 = new JButton("");
		btnColorSecreto_4.setEnabled(false);
		btnColorSecreto_4.setVisible(false);
		btnColorSecreto_4.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_4.setBounds(425, 50, 30, 30);
		contentPane.add(btnColorSecreto_4);

		JButton btnColorSecreto_3 = new JButton("");
		btnColorSecreto_3.setEnabled(false);
		btnColorSecreto_3.setVisible(false);
		btnColorSecreto_3.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_3.setBounds(385, 50, 30, 30);
		contentPane.add(btnColorSecreto_3);

		JButton btnColorSecreto_2 = new JButton("");
		btnColorSecreto_2.setEnabled(false);
		btnColorSecreto_2.setVisible(false);
		btnColorSecreto_2.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_2.setBounds(345, 50, 30, 30);
		contentPane.add(btnColorSecreto_2);

		JButton btnColorSecreto_1 = new JButton("");
		btnColorSecreto_1.setEnabled(false);
		btnColorSecreto_1.setVisible(false);
		btnColorSecreto_1.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_1.setBounds(305, 50, 30, 30);
		contentPane.add(btnColorSecreto_1);

		// Colores asignados de forma random
		btnColorSecreto_1.setBackground(coloresDisponibles.get((int) (Math.random() * 4)));
		btnColorSecreto_2.setBackground(coloresDisponibles.get((int) (Math.random() * 4)));
		btnColorSecreto_3.setBackground(coloresDisponibles.get((int) (Math.random() * 4)));
		btnColorSecreto_4.setBackground(coloresDisponibles.get((int) (Math.random() * 4)));

		// Botones visibles para mostrar en pantalla mientras no se muestran los que
		// contienen la conbinacion Secreta

		JButton btnColorSecreto_4_1 = new JButton("");
		btnColorSecreto_4_1.setEnabled(false);
		btnColorSecreto_4_1.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_4_1.setBounds(425, 50, 30, 30);
		contentPane.add(btnColorSecreto_4_1);

		JButton btnColorSecreto_3_1 = new JButton("");
		btnColorSecreto_3_1.setEnabled(false);
		btnColorSecreto_3_1.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_3_1.setBounds(385, 50, 30, 30);
		contentPane.add(btnColorSecreto_3_1);

		JButton btnColorSecreto_2_1 = new JButton("");
		btnColorSecreto_2_1.setEnabled(false);
		btnColorSecreto_2_1.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_2_1.setBounds(345, 50, 30, 30);
		contentPane.add(btnColorSecreto_2_1);

		JButton btnColorSecreto_1_1 = new JButton("");
		btnColorSecreto_1_1.setEnabled(false);
		btnColorSecreto_1_1.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_1_1.setBounds(305, 50, 30, 30);
		contentPane.add(btnColorSecreto_1_1);

		// Intento 10

		// Botones

		JButton btn1_10 = new JButton("");
		btn1_10.setEnabled(false);
		btn1_10.setBackground(Color.WHITE);
		btn1_10.setBounds(25, 478, 30, 30);
		contentPane.add(btn1_10);

		JButton btn2_10 = new JButton("");
		btn2_10.setEnabled(false);
		btn2_10.setBackground(Color.WHITE);
		btn2_10.setBounds(65, 478, 30, 30);
		contentPane.add(btn2_10);

		JButton btn3_10 = new JButton("");
		btn3_10.setEnabled(false);
		btn3_10.setBackground(Color.WHITE);
		btn3_10.setBounds(105, 478, 30, 30);
		contentPane.add(btn3_10);

		JButton btn4_10 = new JButton("");
		btn4_10.setEnabled(false);
		btn4_10.setBackground(Color.WHITE);
		btn4_10.setBounds(145, 478, 30, 30);
		contentPane.add(btn4_10);

		// Action Botones / Canviar color

		btn1_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_10_color > 3) {
					btn1_10_color = 0;
				}
				btn1_10.setBackground(coloresDisponibles.get(btn1_10_color));
				btn1_10_color++;
			}
		});

		btn2_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_10_color > 3) {
					btn2_10_color = 0;
				}
				btn2_10.setBackground(coloresDisponibles.get(btn2_10_color));
				btn2_10_color++;
			}
		});

		btn3_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_10_color > 3) {
					btn3_10_color = 0;
				}
				btn3_10.setBackground(coloresDisponibles.get(btn3_10_color));
				btn3_10_color++;
			}
		});

		btn4_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_10_color > 3) {
					btn4_10_color = 0;
				}
				btn4_10.setBackground(coloresDisponibles.get(btn4_10_color));
				btn4_10_color++;
			}
		});

		// Comprovacion

		JButton btn4_10_Solucion = new JButton("");
		btn4_10_Solucion.setEnabled(false);
		btn4_10_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_10_Solucion.setBounds(425, 478, 30, 30);
		contentPane.add(btn4_10_Solucion);

		JButton btn3_10_Solucion = new JButton("");
		btn3_10_Solucion.setEnabled(false);
		btn3_10_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_10_Solucion.setBounds(385, 478, 30, 30);
		contentPane.add(btn3_10_Solucion);

		JButton btn2_10_Solucion = new JButton("");
		btn2_10_Solucion.setEnabled(false);
		btn2_10_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_10_Solucion.setBounds(345, 478, 30, 30);
		contentPane.add(btn2_10_Solucion);

		JButton btn1_10_Solucion = new JButton("");
		btn1_10_Solucion.setEnabled(false);
		btn1_10_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_10_Solucion.setBounds(305, 478, 30, 30);
		contentPane.add(btn1_10_Solucion);

		// Boton Comprovar

		JButton btnComprovar10 = new JButton("Comprobar");
		btnComprovar10.setEnabled(false);
		btnComprovar10.setBounds(185, 478, 110, 30);
		contentPane.add(btnComprovar10);

		btnComprovar10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_10.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_10.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_10.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_10.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_10_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_10_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_10_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_10_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_10_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_10_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_10_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_10_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar10.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_10.setEnabled(false);
					btn3_10.setEnabled(false);
					btn2_10.setEnabled(false);
					btn1_10.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");

					// ActivarOpciones
					juegoController.setHaJugadoTrue();

				} else {
					// Sigiente intento
					btnComprovar10.setEnabled(false);

					// Desactivar los botones actuales
					btn4_10.setEnabled(false);
					btn3_10.setEnabled(false);
					btn2_10.setEnabled(false);
					btn1_10.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					JOptionPane.showMessageDialog(null, "Has perdido");
					System.out.println("Has perdido");

					// ActivarOpciones
					juegoController.setHaJugadoTrue();

				}

			}

		});

		// Intento 9

		// Botones

		JButton btn1_9 = new JButton("");
		btn1_9.setEnabled(false);
		btn1_9.setBackground(Color.WHITE);
		btn1_9.setBounds(25, 437, 30, 30);
		contentPane.add(btn1_9);

		JButton btn2_9 = new JButton("");
		btn2_9.setEnabled(false);
		btn2_9.setBackground(Color.WHITE);
		btn2_9.setBounds(65, 437, 30, 30);
		contentPane.add(btn2_9);

		JButton btn3_9 = new JButton("");
		btn3_9.setEnabled(false);
		btn3_9.setBackground(Color.WHITE);
		btn3_9.setBounds(105, 437, 30, 30);
		contentPane.add(btn3_9);

		JButton btn4_9 = new JButton("");
		btn4_9.setEnabled(false);
		btn4_9.setBackground(Color.WHITE);
		btn4_9.setBounds(145, 437, 30, 30);
		contentPane.add(btn4_9);

		// Action Botones / Canviar color

		btn1_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_9_color > 3) {
					btn1_9_color = 0;
				}
				btn1_9.setBackground(coloresDisponibles.get(btn1_9_color));
				btn1_9_color++;
			}
		});

		btn2_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_9_color > 3) {
					btn2_9_color = 0;
				}
				btn2_9.setBackground(coloresDisponibles.get(btn2_9_color));
				btn2_9_color++;
			}
		});

		btn3_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_9_color > 3) {
					btn3_9_color = 0;
				}
				btn3_9.setBackground(coloresDisponibles.get(btn3_9_color));
				btn3_9_color++;
			}
		});

		btn4_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_9_color > 3) {
					btn4_9_color = 0;
				}
				btn4_9.setBackground(coloresDisponibles.get(btn4_9_color));
				btn4_9_color++;
			}
		});

		// Comprovacion

		JButton btn4_9_Solucion = new JButton("");
		btn4_9_Solucion.setEnabled(false);
		btn4_9_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_9_Solucion.setBounds(425, 437, 30, 30);
		contentPane.add(btn4_9_Solucion);

		JButton btn3_9_Solucion = new JButton("");
		btn3_9_Solucion.setEnabled(false);
		btn3_9_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_9_Solucion.setBounds(385, 437, 30, 30);
		contentPane.add(btn3_9_Solucion);

		JButton btn2_9_Solucion = new JButton("");
		btn2_9_Solucion.setEnabled(false);
		btn2_9_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_9_Solucion.setBounds(345, 437, 30, 30);
		contentPane.add(btn2_9_Solucion);

		JButton btn1_9_Solucion = new JButton("");
		btn1_9_Solucion.setEnabled(false);
		btn1_9_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_9_Solucion.setBounds(305, 437, 30, 30);
		contentPane.add(btn1_9_Solucion);

		// Boton Comprovar

		JButton btnComprovar9 = new JButton("Comprobar");
		btnComprovar9.setEnabled(false);
		btnComprovar9.setBounds(185, 437, 110, 30);
		contentPane.add(btnComprovar9);

		btnComprovar9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_9.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_9.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_9.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_9.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_9_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_9_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_9_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_9_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_9_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_9_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_9_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_9_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar9.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_9.setEnabled(false);
					btn3_9.setEnabled(false);
					btn2_9.setEnabled(false);
					btn1_9.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");
					
					// ActivarOpciones
					juegoController.setHaJugadoTrue();
				} else {
					// Sigiente intento
					btnComprovar9.setEnabled(false);
					btnComprovar10.setEnabled(true);

					// Desactivar los botones actuales
					btn4_9.setEnabled(false);
					btn3_9.setEnabled(false);
					btn2_9.setEnabled(false);
					btn1_9.setEnabled(false);

					// Activar los botones siguientes
					btn4_10.setEnabled(true);
					btn3_10.setEnabled(true);
					btn2_10.setEnabled(true);
					btn1_10.setEnabled(true);
				}

			}

		});

		// Intento 8

		// Botones

		JButton btn1_8 = new JButton("");
		btn1_8.setEnabled(false);
		btn1_8.setBackground(Color.WHITE);
		btn1_8.setBounds(25, 396, 30, 30);
		contentPane.add(btn1_8);

		JButton btn2_8 = new JButton("");
		btn2_8.setEnabled(false);
		btn2_8.setBackground(Color.WHITE);
		btn2_8.setBounds(65, 396, 30, 30);
		contentPane.add(btn2_8);

		JButton btn3_8 = new JButton("");
		btn3_8.setEnabled(false);
		btn3_8.setBackground(Color.WHITE);
		btn3_8.setBounds(105, 396, 30, 30);
		contentPane.add(btn3_8);

		JButton btn4_8 = new JButton("");
		btn4_8.setEnabled(false);
		btn4_8.setBackground(Color.WHITE);
		btn4_8.setBounds(145, 396, 30, 30);
		contentPane.add(btn4_8);

		// Action Botones / Canviar color

		btn1_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_8_color > 3) {
					btn1_8_color = 0;
				}
				btn1_8.setBackground(coloresDisponibles.get(btn1_8_color));
				btn1_8_color++;
			}
		});

		btn2_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_8_color > 3) {
					btn2_8_color = 0;
				}
				btn2_8.setBackground(coloresDisponibles.get(btn2_8_color));
				btn2_8_color++;
			}
		});

		btn3_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_8_color > 3) {
					btn3_8_color = 0;
				}
				btn3_8.setBackground(coloresDisponibles.get(btn3_8_color));
				btn3_8_color++;
			}
		});

		btn4_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_8_color > 3) {
					btn4_8_color = 0;
				}
				btn4_8.setBackground(coloresDisponibles.get(btn4_8_color));
				btn4_8_color++;
			}
		});

		// Comprovacion

		JButton btn4_8_Solucion = new JButton("");
		btn4_8_Solucion.setEnabled(false);
		btn4_8_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_8_Solucion.setBounds(425, 396, 30, 30);
		contentPane.add(btn4_8_Solucion);

		JButton btn3_8_Solucion = new JButton("");
		btn3_8_Solucion.setEnabled(false);
		btn3_8_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_8_Solucion.setBounds(385, 396, 30, 30);
		contentPane.add(btn3_8_Solucion);

		JButton btn2_8_Solucion = new JButton("");
		btn2_8_Solucion.setEnabled(false);
		btn2_8_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_8_Solucion.setBounds(345, 396, 30, 30);
		contentPane.add(btn2_8_Solucion);

		JButton btn1_8_Solucion = new JButton("");
		btn1_8_Solucion.setEnabled(false);
		btn1_8_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_8_Solucion.setBounds(305, 396, 30, 30);
		contentPane.add(btn1_8_Solucion);

		// Boton Comprovar

		JButton btnComprovar8 = new JButton("Comprobar");
		btnComprovar8.setEnabled(false);
		btnComprovar8.setBounds(185, 396, 110, 30);
		contentPane.add(btnComprovar8);

		btnComprovar8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_8.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_8.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_8.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_8.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_8_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_8_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_8_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_8_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_8_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_8_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_8_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_8_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar8.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_8.setEnabled(false);
					btn3_8.setEnabled(false);
					btn2_8.setEnabled(false);
					btn1_8.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");
					
					// ActivarOpciones
					juegoController.setHaJugadoTrue();
				} else {
					// Sigiente intento
					btnComprovar8.setEnabled(false);
					btnComprovar9.setEnabled(true);

					// Desactivar los botones actuales
					btn4_8.setEnabled(false);
					btn3_8.setEnabled(false);
					btn2_8.setEnabled(false);
					btn1_8.setEnabled(false);

					// Activar los botones siguientes
					btn4_9.setEnabled(true);
					btn3_9.setEnabled(true);
					btn2_9.setEnabled(true);
					btn1_9.setEnabled(true);
				}

			}

		});

		// Intento 7

		// Botones

		JButton btn1_7 = new JButton("");
		btn1_7.setEnabled(false);
		btn1_7.setBackground(Color.WHITE);
		btn1_7.setBounds(25, 355, 30, 30);
		contentPane.add(btn1_7);

		JButton btn2_7 = new JButton("");
		btn2_7.setEnabled(false);
		btn2_7.setBackground(Color.WHITE);
		btn2_7.setBounds(65, 355, 30, 30);
		contentPane.add(btn2_7);

		JButton btn3_7 = new JButton("");
		btn3_7.setEnabled(false);
		btn3_7.setBackground(Color.WHITE);
		btn3_7.setBounds(105, 355, 30, 30);
		contentPane.add(btn3_7);

		JButton btn4_7 = new JButton("");
		btn4_7.setEnabled(false);
		btn4_7.setBackground(Color.WHITE);
		btn4_7.setBounds(145, 355, 30, 30);
		contentPane.add(btn4_7);

		// Action Botones / Canviar color

		btn1_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_7_color > 3) {
					btn1_7_color = 0;
				}
				btn1_7.setBackground(coloresDisponibles.get(btn1_7_color));
				btn1_7_color++;
			}
		});

		btn2_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_7_color > 3) {
					btn2_7_color = 0;
				}
				btn2_7.setBackground(coloresDisponibles.get(btn2_7_color));
				btn2_7_color++;
			}
		});

		btn3_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_7_color > 3) {
					btn3_7_color = 0;
				}
				btn3_7.setBackground(coloresDisponibles.get(btn3_7_color));
				btn3_7_color++;
			}
		});

		btn4_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_7_color > 3) {
					btn4_7_color = 0;
				}
				btn4_7.setBackground(coloresDisponibles.get(btn4_7_color));
				btn4_7_color++;
			}
		});

		// Comprovacion

		JButton btn4_7_Solucion = new JButton("");
		btn4_7_Solucion.setEnabled(false);
		btn4_7_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_7_Solucion.setBounds(425, 355, 30, 30);
		contentPane.add(btn4_7_Solucion);

		JButton btn3_7_Solucion = new JButton("");
		btn3_7_Solucion.setEnabled(false);
		btn3_7_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_7_Solucion.setBounds(385, 355, 30, 30);
		contentPane.add(btn3_7_Solucion);

		JButton btn2_7_Solucion = new JButton("");
		btn2_7_Solucion.setEnabled(false);
		btn2_7_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_7_Solucion.setBounds(345, 355, 30, 30);
		contentPane.add(btn2_7_Solucion);

		JButton btn1_7_Solucion = new JButton("");
		btn1_7_Solucion.setEnabled(false);
		btn1_7_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_7_Solucion.setBounds(305, 355, 30, 30);
		contentPane.add(btn1_7_Solucion);

		// Boton Comprovar

		JButton btnComprovar7 = new JButton("Comprobar");
		btnComprovar7.setEnabled(false);
		btnComprovar7.setBounds(185, 355, 110, 30);
		contentPane.add(btnComprovar7);

		btnComprovar7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_7.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_7.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_7.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_7.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_7_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_7_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_7_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_7_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_7_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_7_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_7_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_7_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar7.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_7.setEnabled(false);
					btn3_7.setEnabled(false);
					btn2_7.setEnabled(false);
					btn1_7.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");
					
					// ActivarOpciones
					juegoController.setHaJugadoTrue();
				} else {
					// Sigiente intento
					btnComprovar7.setEnabled(false);
					btnComprovar8.setEnabled(true);

					// Desactivar los botones actuales
					btn4_7.setEnabled(false);
					btn3_7.setEnabled(false);
					btn2_7.setEnabled(false);
					btn1_7.setEnabled(false);

					// Activar los botones siguientes
					btn4_8.setEnabled(true);
					btn3_8.setEnabled(true);
					btn2_8.setEnabled(true);
					btn1_8.setEnabled(true);
				}

			}

		});

		// Intento 6

		// Botones

		JButton btn1_6 = new JButton("");
		btn1_6.setEnabled(false);
		btn1_6.setBackground(Color.WHITE);
		btn1_6.setBounds(25, 314, 30, 30);
		contentPane.add(btn1_6);

		JButton btn2_6 = new JButton("");
		btn2_6.setEnabled(false);
		btn2_6.setBackground(Color.WHITE);
		btn2_6.setBounds(65, 314, 30, 30);
		contentPane.add(btn2_6);

		JButton btn3_6 = new JButton("");
		btn3_6.setEnabled(false);
		btn3_6.setBackground(Color.WHITE);
		btn3_6.setBounds(105, 314, 30, 30);
		contentPane.add(btn3_6);

		JButton btn4_6 = new JButton("");
		btn4_6.setEnabled(false);
		btn4_6.setBackground(Color.WHITE);
		btn4_6.setBounds(145, 314, 30, 30);
		contentPane.add(btn4_6);

		// Action Botones / Canviar color

		btn1_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_6_color > 3) {
					btn1_6_color = 0;
				}
				btn1_6.setBackground(coloresDisponibles.get(btn1_6_color));
				btn1_6_color++;
			}
		});

		btn2_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_6_color > 3) {
					btn2_6_color = 0;
				}
				btn2_6.setBackground(coloresDisponibles.get(btn2_6_color));
				btn2_6_color++;
			}
		});

		btn3_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_6_color > 3) {
					btn3_6_color = 0;
				}
				btn3_6.setBackground(coloresDisponibles.get(btn3_6_color));
				btn3_6_color++;
			}
		});

		btn4_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_6_color > 3) {
					btn4_6_color = 0;
				}
				btn4_6.setBackground(coloresDisponibles.get(btn4_6_color));
				btn4_6_color++;
			}
		});

		// Comprovacion

		JButton btn4_6_Solucion = new JButton("");
		btn4_6_Solucion.setEnabled(false);
		btn4_6_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_6_Solucion.setBounds(425, 314, 30, 30);
		contentPane.add(btn4_6_Solucion);

		JButton btn3_6_Solucion = new JButton("");
		btn3_6_Solucion.setEnabled(false);
		btn3_6_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_6_Solucion.setBounds(385, 314, 30, 30);
		contentPane.add(btn3_6_Solucion);

		JButton btn2_6_Solucion = new JButton("");
		btn2_6_Solucion.setEnabled(false);
		btn2_6_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_6_Solucion.setBounds(345, 314, 30, 30);
		contentPane.add(btn2_6_Solucion);

		JButton btn1_6_Solucion = new JButton("");
		btn1_6_Solucion.setEnabled(false);
		btn1_6_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_6_Solucion.setBounds(305, 314, 30, 30);
		contentPane.add(btn1_6_Solucion);

		// Boton Comprovar

		JButton btnComprovar6 = new JButton("Comprobar");
		btnComprovar6.setEnabled(false);
		btnComprovar6.setBounds(185, 314, 110, 30);
		contentPane.add(btnComprovar6);

		btnComprovar6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_6.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_6.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_6.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_6.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_6_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_6_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_6_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_6_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_6_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_6_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_6_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_6_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar6.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_6.setEnabled(false);
					btn3_6.setEnabled(false);
					btn2_6.setEnabled(false);
					btn1_6.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");
					
					// ActivarOpciones
					juegoController.setHaJugadoTrue();
				} else {
					// Sigiente intento
					btnComprovar6.setEnabled(false);
					btnComprovar7.setEnabled(true);

					// Desactivar los botones actuales
					btn4_6.setEnabled(false);
					btn3_6.setEnabled(false);
					btn2_6.setEnabled(false);
					btn1_6.setEnabled(false);

					// Activar los botones siguientes
					btn4_7.setEnabled(true);
					btn3_7.setEnabled(true);
					btn2_7.setEnabled(true);
					btn1_7.setEnabled(true);
				}

			}

		});

		// Intento 5

		// Botones

		JButton btn1_5 = new JButton("");
		btn1_5.setEnabled(false);
		btn1_5.setBackground(Color.WHITE);
		btn1_5.setBounds(25, 273, 30, 30);
		contentPane.add(btn1_5);

		JButton btn2_5 = new JButton("");
		btn2_5.setEnabled(false);
		btn2_5.setBackground(Color.WHITE);
		btn2_5.setBounds(65, 273, 30, 30);
		contentPane.add(btn2_5);

		JButton btn3_5 = new JButton("");
		btn3_5.setEnabled(false);
		btn3_5.setBackground(Color.WHITE);
		btn3_5.setBounds(105, 273, 30, 30);
		contentPane.add(btn3_5);

		JButton btn4_5 = new JButton("");
		btn4_5.setEnabled(false);
		btn4_5.setBackground(Color.WHITE);
		btn4_5.setBounds(145, 273, 30, 30);
		contentPane.add(btn4_5);

		// Action Botones / Canviar color

		btn1_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_5_color > 3) {
					btn1_5_color = 0;
				}
				btn1_5.setBackground(coloresDisponibles.get(btn1_5_color));
				btn1_5_color++;
			}
		});

		btn2_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_5_color > 3) {
					btn2_5_color = 0;
				}
				btn2_5.setBackground(coloresDisponibles.get(btn2_5_color));
				btn2_5_color++;
			}
		});

		btn3_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_5_color > 3) {
					btn3_5_color = 0;
				}
				btn3_5.setBackground(coloresDisponibles.get(btn3_5_color));
				btn3_5_color++;
			}
		});

		btn4_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_5_color > 3) {
					btn4_5_color = 0;
				}
				btn4_5.setBackground(coloresDisponibles.get(btn4_5_color));
				btn4_5_color++;
			}
		});

		// Comprovacion

		JButton btn4_5_Solucion = new JButton("");
		btn4_5_Solucion.setEnabled(false);
		btn4_5_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_5_Solucion.setBounds(425, 273, 30, 30);
		contentPane.add(btn4_5_Solucion);

		JButton btn3_5_Solucion = new JButton("");
		btn3_5_Solucion.setEnabled(false);
		btn3_5_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_5_Solucion.setBounds(385, 273, 30, 30);
		contentPane.add(btn3_5_Solucion);

		JButton btn2_5_Solucion = new JButton("");
		btn2_5_Solucion.setEnabled(false);
		btn2_5_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_5_Solucion.setBounds(345, 273, 30, 30);
		contentPane.add(btn2_5_Solucion);

		JButton btn1_5_Solucion = new JButton("");
		btn1_5_Solucion.setEnabled(false);
		btn1_5_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_5_Solucion.setBounds(305, 273, 30, 30);
		contentPane.add(btn1_5_Solucion);

		// Boton Comprovar

		JButton btnComprovar5 = new JButton("Comprobar");
		btnComprovar5.setEnabled(false);
		btnComprovar5.setBounds(185, 273, 110, 30);
		contentPane.add(btnComprovar5);

		btnComprovar5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_5.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_5.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_5.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_5.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_5_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_5_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_5_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_5_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_5_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_5_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_5_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_5_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar5.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_5.setEnabled(false);
					btn3_5.setEnabled(false);
					btn2_5.setEnabled(false);
					btn1_5.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");
					
					// ActivarOpciones
					juegoController.setHaJugadoTrue();
				} else {
					// Sigiente intento
					btnComprovar5.setEnabled(false);
					btnComprovar6.setEnabled(true);

					// btnComprovar6.setEnabled(true);

					// Desactivar los botones actuales
					btn4_5.setEnabled(false);
					btn3_5.setEnabled(false);
					btn2_5.setEnabled(false);
					btn1_5.setEnabled(false);

					// Activar los botones siguientes
					btn4_6.setEnabled(true);
					btn3_6.setEnabled(true);
					btn2_6.setEnabled(true);
					btn1_6.setEnabled(true);

				}

			}

		});

		// Intento 4

		// Botones

		JButton btn1_4 = new JButton("");
		btn1_4.setEnabled(false);
		btn1_4.setBackground(Color.WHITE);
		btn1_4.setBounds(25, 232, 30, 30);
		contentPane.add(btn1_4);

		JButton btn2_4 = new JButton("");
		btn2_4.setEnabled(false);
		btn2_4.setBackground(Color.WHITE);
		btn2_4.setBounds(65, 232, 30, 30);
		contentPane.add(btn2_4);

		JButton btn3_4 = new JButton("");
		btn3_4.setEnabled(false);
		btn3_4.setBackground(Color.WHITE);
		btn3_4.setBounds(105, 232, 30, 30);
		contentPane.add(btn3_4);

		JButton btn4_4 = new JButton("");
		btn4_4.setEnabled(false);
		btn4_4.setBackground(Color.WHITE);
		btn4_4.setBounds(145, 232, 30, 30);
		contentPane.add(btn4_4);

		// Action Botones / Canviar color

		btn1_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_4_color > 3) {
					btn1_4_color = 0;
				}
				btn1_4.setBackground(coloresDisponibles.get(btn1_4_color));
				btn1_4_color++;
			}
		});

		btn2_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_4_color > 3) {
					btn2_4_color = 0;
				}
				btn2_4.setBackground(coloresDisponibles.get(btn2_4_color));
				btn2_4_color++;
			}
		});

		btn3_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_4_color > 3) {
					btn3_4_color = 0;
				}
				btn3_4.setBackground(coloresDisponibles.get(btn3_4_color));
				btn3_4_color++;
			}
		});

		btn4_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_4_color > 3) {
					btn4_4_color = 0;
				}
				btn4_4.setBackground(coloresDisponibles.get(btn4_4_color));
				btn4_4_color++;
			}
		});

		// Comprovacion

		JButton btn4_4_Solucion = new JButton("");
		btn4_4_Solucion.setEnabled(false);
		btn4_4_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_4_Solucion.setBounds(425, 232, 30, 30);
		contentPane.add(btn4_4_Solucion);

		JButton btn3_4_Solucion = new JButton("");
		btn3_4_Solucion.setEnabled(false);
		btn3_4_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_4_Solucion.setBounds(385, 232, 30, 30);
		contentPane.add(btn3_4_Solucion);

		JButton btn2_4_Solucion = new JButton("");
		btn2_4_Solucion.setEnabled(false);
		btn2_4_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_4_Solucion.setBounds(345, 232, 30, 30);
		contentPane.add(btn2_4_Solucion);

		JButton btn1_4_Solucion = new JButton("");
		btn1_4_Solucion.setEnabled(false);
		btn1_4_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_4_Solucion.setBounds(305, 232, 30, 30);
		contentPane.add(btn1_4_Solucion);

		// Boton Comprovar

		JButton btnComprovar4 = new JButton("Comprobar");
		btnComprovar4.setEnabled(false);
		btnComprovar4.setBounds(185, 232, 110, 30);
		contentPane.add(btnComprovar4);

		btnComprovar4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_4.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_4.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_4.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_4.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_4_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_4_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_4_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_4_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_4_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_4_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_4_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_4_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar4.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_4.setEnabled(false);
					btn3_4.setEnabled(false);
					btn2_4.setEnabled(false);
					btn1_4.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");
					
					// ActivarOpciones
					juegoController.setHaJugadoTrue();
				} else {
					// Sigiente intento
					btnComprovar4.setEnabled(false);
					btnComprovar5.setEnabled(true);

					// Desactivar los botones actuales
					btn4_4.setEnabled(false);
					btn3_4.setEnabled(false);
					btn2_4.setEnabled(false);
					btn1_4.setEnabled(false);

					// Activar los botones siguientes
					btn4_5.setEnabled(true);
					btn3_5.setEnabled(true);
					btn2_5.setEnabled(true);
					btn1_5.setEnabled(true);
				}

			}

		});

		// Intento 3

		// Botones

		JButton btn1_3 = new JButton("");
		btn1_3.setEnabled(false);
		btn1_3.setBackground(Color.WHITE);
		btn1_3.setBounds(25, 191, 30, 30);
		contentPane.add(btn1_3);

		JButton btn2_3 = new JButton("");
		btn2_3.setEnabled(false);
		btn2_3.setBackground(Color.WHITE);
		btn2_3.setBounds(65, 191, 30, 30);
		contentPane.add(btn2_3);

		JButton btn3_3 = new JButton("");
		btn3_3.setEnabled(false);
		btn3_3.setBackground(Color.WHITE);
		btn3_3.setBounds(105, 191, 30, 30);
		contentPane.add(btn3_3);

		JButton btn4_3 = new JButton("");
		btn4_3.setEnabled(false);
		btn4_3.setBackground(Color.WHITE);
		btn4_3.setBounds(145, 191, 30, 30);
		contentPane.add(btn4_3);

		// Action Botones / Canviar color

		btn1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_3_color > 3) {
					btn1_3_color = 0;
				}
				btn1_3.setBackground(coloresDisponibles.get(btn1_3_color));
				btn1_3_color++;
			}
		});

		btn2_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_3_color > 3) {
					btn2_3_color = 0;
				}
				btn2_3.setBackground(coloresDisponibles.get(btn2_3_color));
				btn2_3_color++;
			}
		});

		btn3_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_3_color > 3) {
					btn3_3_color = 0;
				}
				btn3_3.setBackground(coloresDisponibles.get(btn3_3_color));
				btn3_3_color++;
			}
		});

		btn4_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_3_color > 3) {
					btn4_3_color = 0;
				}
				btn4_3.setBackground(coloresDisponibles.get(btn4_3_color));
				btn4_3_color++;
			}
		});

		// Comprovacion

		JButton btn4_3_Solucion = new JButton("");
		btn4_3_Solucion.setEnabled(false);
		btn4_3_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_3_Solucion.setBounds(425, 191, 30, 30);
		contentPane.add(btn4_3_Solucion);

		JButton btn3_3_Solucion = new JButton("");
		btn3_3_Solucion.setEnabled(false);
		btn3_3_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_3_Solucion.setBounds(385, 191, 30, 30);
		contentPane.add(btn3_3_Solucion);

		JButton btn2_3_Solucion = new JButton("");
		btn2_3_Solucion.setEnabled(false);
		btn2_3_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_3_Solucion.setBounds(345, 191, 30, 30);
		contentPane.add(btn2_3_Solucion);

		JButton btn1_3_Solucion = new JButton("");
		btn1_3_Solucion.setEnabled(false);
		btn1_3_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_3_Solucion.setBounds(305, 191, 30, 30);
		contentPane.add(btn1_3_Solucion);

		// Boton Comprovar

		JButton btnComprovar3 = new JButton("Comprobar");
		btnComprovar3.setEnabled(false);
		btnComprovar3.setBounds(185, 191, 110, 30);
		contentPane.add(btnComprovar3);

		btnComprovar3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_3.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_3.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_3.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_3.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_3_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_3_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_3_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_3_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_3_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_3_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_3_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_3_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar3.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_3.setEnabled(false);
					btn3_3.setEnabled(false);
					btn2_3.setEnabled(false);
					btn1_3.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");
					
					// ActivarOpciones
					juegoController.setHaJugadoTrue();
				} else {
					// Sigiente intento
					btnComprovar3.setEnabled(false);
					btnComprovar4.setEnabled(true);

					// Desactivar los botones actuales
					btn4_3.setEnabled(false);
					btn3_3.setEnabled(false);
					btn2_3.setEnabled(false);
					btn1_3.setEnabled(false);

					// Activar los botones siguientes
					btn4_4.setEnabled(true);
					btn3_4.setEnabled(true);
					btn2_4.setEnabled(true);
					btn1_4.setEnabled(true);

				}

			}

		});

		// Intento 2

		// Botones

		JButton btn1_2 = new JButton("");
		btn1_2.setEnabled(false);
		btn1_2.setBackground(Color.WHITE);
		btn1_2.setBounds(25, 150, 30, 30);
		contentPane.add(btn1_2);

		JButton btn2_2 = new JButton("");
		btn2_2.setEnabled(false);
		btn2_2.setBackground(Color.WHITE);
		btn2_2.setBounds(65, 150, 30, 30);
		contentPane.add(btn2_2);

		JButton btn3_2 = new JButton("");
		btn3_2.setEnabled(false);
		btn3_2.setBackground(Color.WHITE);
		btn3_2.setBounds(105, 150, 30, 30);
		contentPane.add(btn3_2);

		JButton btn4_2 = new JButton("");
		btn4_2.setEnabled(false);
		btn4_2.setBackground(Color.WHITE);
		btn4_2.setBounds(145, 150, 30, 30);
		contentPane.add(btn4_2);

		// Action Botones / Canviar color

		btn1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_2_color > 3) {
					btn1_2_color = 0;
				}
				btn1_2.setBackground(coloresDisponibles.get(btn1_2_color));
				btn1_2_color++;
			}
		});

		btn2_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_2_color > 3) {
					btn2_2_color = 0;
				}
				btn2_2.setBackground(coloresDisponibles.get(btn2_2_color));
				btn2_2_color++;
			}
		});

		btn3_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_2_color > 3) {
					btn3_2_color = 0;
				}
				btn3_2.setBackground(coloresDisponibles.get(btn3_2_color));
				btn3_2_color++;
			}
		});

		btn4_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_2_color > 3) {
					btn4_2_color = 0;
				}
				btn4_2.setBackground(coloresDisponibles.get(btn4_2_color));
				btn4_2_color++;
			}
		});

		// Comprovacion

		JButton btn4_2_Solucion = new JButton("");
		btn4_2_Solucion.setEnabled(false);
		btn4_2_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_2_Solucion.setBounds(425, 150, 30, 30);
		contentPane.add(btn4_2_Solucion);

		JButton btn3_2_Solucion = new JButton("");
		btn3_2_Solucion.setEnabled(false);
		btn3_2_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_2_Solucion.setBounds(385, 150, 30, 30);
		contentPane.add(btn3_2_Solucion);

		JButton btn2_2_Solucion = new JButton("");
		btn2_2_Solucion.setEnabled(false);
		btn2_2_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_2_Solucion.setBounds(345, 150, 30, 30);
		contentPane.add(btn2_2_Solucion);

		JButton btn1_2_Solucion = new JButton("");
		btn1_2_Solucion.setEnabled(false);
		btn1_2_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_2_Solucion.setBounds(305, 150, 30, 30);
		contentPane.add(btn1_2_Solucion);

		// Boton Comprovar

		JButton btnComprovar2 = new JButton("Comprobar");
		btnComprovar2.setEnabled(false);
		btnComprovar2.setBounds(185, 150, 110, 30);
		contentPane.add(btnComprovar2);

		btnComprovar2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_2.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_2.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_2.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_2.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_2_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_2_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_2_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_2_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_2_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_2_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_2_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_2_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar2.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_2.setEnabled(false);
					btn3_2.setEnabled(false);
					btn2_2.setEnabled(false);
					btn1_2.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");
					
					// ActivarOpciones
					juegoController.setHaJugadoTrue();
				} else {
					// Sigiente intento
					btnComprovar2.setEnabled(false);
					btnComprovar3.setEnabled(true);

					// Desactivar los botones actuales
					btn4_2.setEnabled(false);
					btn3_2.setEnabled(false);
					btn2_2.setEnabled(false);
					btn1_2.setEnabled(false);

					// Activar los botones siguientes
					btn4_3.setEnabled(true);
					btn3_3.setEnabled(true);
					btn2_3.setEnabled(true);
					btn1_3.setEnabled(true);

				}

			}

		});

		// Intento 1

		// Botones

		JButton btn1_1 = new JButton("");
		btn1_1.setBackground(Color.WHITE);
		btn1_1.setBounds(25, 110, 30, 30);
		contentPane.add(btn1_1);

		JButton btn2_1 = new JButton("");
		btn2_1.setBackground(Color.WHITE);
		btn2_1.setBounds(65, 110, 30, 30);
		contentPane.add(btn2_1);

		JButton btn3_1 = new JButton("");
		btn3_1.setBackground(Color.WHITE);
		btn3_1.setBounds(105, 110, 30, 30);
		contentPane.add(btn3_1);

		JButton btn4_1 = new JButton("");
		btn4_1.setBackground(Color.WHITE);
		btn4_1.setBounds(145, 110, 30, 30);
		contentPane.add(btn4_1);

		// Action Botones / Canviar color

		btn1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_1_color > 3) {
					btn1_1_color = 0;
				}
				btn1_1.setBackground(coloresDisponibles.get(btn1_1_color));
				btn1_1_color++;
			}
		});

		btn2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_1_color > 3) {
					btn2_1_color = 0;
				}
				btn2_1.setBackground(coloresDisponibles.get(btn2_1_color));
				btn2_1_color++;
			}
		});

		btn3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_1_color > 3) {
					btn3_1_color = 0;
				}
				btn3_1.setBackground(coloresDisponibles.get(btn3_1_color));
				btn3_1_color++;
			}
		});

		btn4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_1_color > 3) {
					btn4_1_color = 0;
				}
				btn4_1.setBackground(coloresDisponibles.get(btn4_1_color));
				btn4_1_color++;
			}
		});

		// Comprovacion

		JButton btn4_1_Solucion = new JButton("");
		btn4_1_Solucion.setEnabled(false);
		btn4_1_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_1_Solucion.setBounds(425, 110, 30, 30);
		contentPane.add(btn4_1_Solucion);

		JButton btn3_1_Solucion = new JButton("");
		btn3_1_Solucion.setEnabled(false);
		btn3_1_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_1_Solucion.setBounds(385, 110, 30, 30);
		contentPane.add(btn3_1_Solucion);

		JButton btn2_1_Solucion = new JButton("");
		btn2_1_Solucion.setEnabled(false);
		btn2_1_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_1_Solucion.setBounds(345, 110, 30, 30);
		contentPane.add(btn2_1_Solucion);

		JButton btn1_1_Solucion = new JButton("");
		btn1_1_Solucion.setEnabled(false);
		btn1_1_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_1_Solucion.setBounds(305, 110, 30, 30);
		contentPane.add(btn1_1_Solucion);

		// Boton Comprovar

		JButton btnComprovar1 = new JButton("Comprobar");
		btnComprovar1.setEnabled(true);
		btnComprovar1.setBounds(185, 110, 110, 30);
		contentPane.add(btnComprovar1);

		btnComprovar1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color color1 = btn1_1.getBackground();
				Color colorSecreto1 = btnColorSecreto_1.getBackground();
				Color color2 = btn2_1.getBackground();
				Color colorSecreto2 = btnColorSecreto_2.getBackground();
				Color color3 = btn3_1.getBackground();
				Color colorSecreto3 = btnColorSecreto_3.getBackground();
				Color color4 = btn4_1.getBackground();
				Color colorSecreto4 = btnColorSecreto_4.getBackground();

				ArrayList<Integer> numerosCasillas = new ArrayList<Integer>();
				numerosCasillas = comprovarColores(color1, colorSecreto1, color2, colorSecreto2, color3, colorSecreto3, color4, colorSecreto4);

				int coloresCorrectos = numerosCasillas.get(0);
				int coloresCorrectosMalSituados = numerosCasillas.get(1);

				
				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (coloresCorrectos > control1) {
					if (botonesPintados == 0) {
						btn1_1_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_1_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_1_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_1_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (coloresCorrectosMalSituados > control2) {
					if (botonesPintados == 0) {
						btn1_1_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_1_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_1_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_1_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (coloresCorrectos == 4) {
					// Has ganado
					btnComprovar1.setEnabled(false);

					// Mostrar la combinacion
					btnColorSecreto_4.setVisible(true);
					btnColorSecreto_3.setVisible(true);
					btnColorSecreto_2.setVisible(true);
					btnColorSecreto_1.setVisible(true);

					btnColorSecreto_4_1.setVisible(false);
					btnColorSecreto_3_1.setVisible(false);
					btnColorSecreto_2_1.setVisible(false);
					btnColorSecreto_1_1.setVisible(false);

					// Desactivar los botones actuales
					btn4_1.setEnabled(false);
					btn3_1.setEnabled(false);
					btn2_1.setEnabled(false);
					btn1_1.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					System.out.println("¡Felicidades, has ganado!");
					
					// ActivarOpciones
					juegoController.setHaJugadoTrue();
				} else {
					// Sigiente intento
					btnComprovar1.setEnabled(false);
					btnComprovar2.setEnabled(true);

					// Desactivar los botones actuales
					btn4_1.setEnabled(false);
					btn3_1.setEnabled(false);
					btn2_1.setEnabled(false);
					btn1_1.setEnabled(false);

					// Activar los botones siguientes
					btn4_2.setEnabled(true);
					btn3_2.setEnabled(true);
					btn2_2.setEnabled(true);
					btn1_2.setEnabled(true);
				}

			}

		});

	}

	public ArrayList comprovarColores(Color color1, Color colorSecreto1, Color color2, Color colorSecreto2, Color color3, Color colorSecreto3, Color color4, Color colorSecreto4) {
		ArrayList<Integer> numeros = new ArrayList<Integer>();
		
		int coloresCorrectos = 0;
		int coloresCorrectosMalSituados = 0;
		
		if (color1 == colorSecreto1) {
			coloresCorrectos++;
		} else if (color1 == colorSecreto2 || color1 == colorSecreto3 || color1 == colorSecreto4) {
			coloresCorrectosMalSituados++;
		}
		
		if (color2 == colorSecreto2) {
			coloresCorrectos++;
		} else if (color2 == colorSecreto1 || color2 == colorSecreto3 || color2 == colorSecreto4) {
			coloresCorrectosMalSituados++;
		}
		
		if (color3 == colorSecreto3) {
			coloresCorrectos++;
		} else if (color3 == colorSecreto1 || color3 == colorSecreto2 || color3 == colorSecreto4) {
			coloresCorrectosMalSituados++;
		}
		
		if (color4 == colorSecreto4) {
			coloresCorrectos++;
		} else if (color4 == colorSecreto1 || color4 == colorSecreto2 || color4 == colorSecreto3) {
			coloresCorrectosMalSituados++;
		}

		System.out.println("Casillas Negras " + coloresCorrectos);
		System.out.println("Casillas Blancas " + coloresCorrectosMalSituados);
		
		numeros.add(coloresCorrectos);
		numeros.add(coloresCorrectosMalSituados);
		
		return numeros;
	}
	
	public void setCordinador(JuegoController juegoController) {
		this.juegoController = juegoController;

	}
}
