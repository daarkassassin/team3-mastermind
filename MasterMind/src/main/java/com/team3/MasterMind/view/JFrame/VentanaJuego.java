package com.team3.MasterMind.view.JFrame;

import javax.swing.*;

import com.team3.MasterMind.AppMain.MasterMindApp;
import com.team3.MasterMind.controller.JuegoController;
import com.team3.MasterMind.view.JPanel.PanelAvanzado;
import com.team3.MasterMind.view.JPanel.PanelMedio;
import com.team3.MasterMind.view.JPanel.PanelPrincipiante;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VentanaJuego extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private boolean haJugado = false;
	private JuegoController juegoController;

	private JMenuBar menuBar;
	private JMenu mnJuego, mnAyuda;
	private JMenuItem mntmNuevoJuego, mntmOpciones, mntmSalir, mntmAyuda;

	private PanelPrincipiante panelPrincipiante;
	private PanelMedio panelMedio;
	private PanelAvanzado panelAvanzado;
	
	public VentanaJuego() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Menu de la aplicación
		menuBar = new JMenuBar();
		mnJuego = new JMenu("Juego");
		menuBar.add(mnJuego);

		mntmNuevoJuego = new JMenuItem("NuevoJuego");
		mntmNuevoJuego.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		mnJuego.add(mntmNuevoJuego);

		mntmOpciones = new JMenuItem("Opciones");
		mntmOpciones.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		mntmOpciones.setEnabled(false);
		
		mnJuego.add(mntmOpciones);

		mntmSalir = new JMenuItem("Salir");
		mntmSalir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0));
		mnJuego.add(mntmSalir);

		mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);

		mntmAyuda = new JMenuItem("Ayuda");
		mntmAyuda.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		mnAyuda.add(mntmAyuda);

		mntmNuevoJuego.addActionListener(this);
		mntmOpciones.addActionListener(this);
		mntmAyuda.addActionListener(this);
		mntmSalir.addActionListener(this);

		// JButton Aceptar / Cancelar
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(65, 209, 122, 43);
		getContentPane().add(btnAceptar);
		
		// Activamos / Desactivamos las opciones
		activarOpciones();
		
		setTitle("MasterMind");
		setJMenuBar(menuBar);
		setSize(700, 700);
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mntmNuevoJuego) {
			if (MasterMindApp.debug)
				System.out.println("NuevoJuego");
			juegoController.mostrarVentanaNivel();
		}
		if (e.getSource() == mntmOpciones) {
			if (MasterMindApp.debug)
				System.out.println("Opciones");
			juegoController.mostrarVentanaOpciones();
		}
		if (e.getSource() == mntmSalir) {
			if (MasterMindApp.debug)
				System.out.println("Salir");
			System.exit(0);
		}
		if (e.getSource() == mntmAyuda) {
			if (MasterMindApp.debug)
				System.out.println("Ayuda");
			JOptionPane.showMessageDialog(null,
					"|| Made by Team3 || MASTER MIND || Ver " + MasterMindApp.VERSION + " ||");
		}
	}
	public void activarOpciones() {
		if(haJugado) {
			mntmOpciones.setEnabled(true);
		}else {
			mntmOpciones.setEnabled(false);
		}
		if (MasterMindApp.debug)mntmOpciones.setEnabled(true);
	}
	public void setHaJugado(boolean haJugado) {
		this.haJugado = haJugado;
	}

	public void setCordinador(JuegoController juegoController) {
		this.juegoController = juegoController;
	}

	public void setPanelPrincipiante(PanelPrincipiante panel) {
		this.panelPrincipiante = panel;
		this.getContentPane().removeAll();
		this.setContentPane(panelPrincipiante.contentPane);
		this.revalidate();
	}

	public void setPanelMedio(PanelMedio panel) {
		this.panelMedio = panel;
		this.getContentPane().removeAll();
		this.setContentPane(panelMedio.contentPane);
		this.revalidate();
	}

	public void setPanelAvanzado(PanelAvanzado panel) {
		this.panelAvanzado = panel;
		this.getContentPane().removeAll();
		this.setContentPane(panelAvanzado.contentPane);
		this.revalidate();
	}
	

}
