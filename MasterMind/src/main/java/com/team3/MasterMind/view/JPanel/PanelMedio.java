package com.team3.MasterMind.view.JPanel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;

import com.team3.MasterMind.AppMain.MasterMindApp;
import com.team3.MasterMind.controller.JuegoController;
import com.team3.MasterMind.model.dto.Colores;

public class PanelMedio extends JPanel {
	private static final long serialVersionUID = 1L;
	JuegoController juegoController;

	public JPanel contentPane;
	public Random r = new Random(); // Random
	public ArrayList<Color> coloresDisponibles = new ArrayList<Color>(); // Array colores

	public Color solucion1;
	public Color solucion2;
	public Color solucion3;
	public Color solucion4;

	// Botones de la aplicacion
	public JButton btnColorSecreto_1, btnColorSecreto_2, btnColorSecreto_3, btnColorSecreto_4, btn1_1, btn2_1, btn3_1,
			btn4_1, btnComprovar1, btn1_1_Solucion, btn2_1_Solucion, btn3_1_Solucion, btn4_1_Solucion, btn1_2, btn2_2,
			btn3_2, btn4_2, btnComprovar2, btn1_2_Solucion, btn2_2_Solucion, btn3_2_Solucion, btn4_2_Solucion, btn1_3,
			btn2_3, btn3_3, btn4_3, btnComprovar3, btn1_3_Solucion, btn2_3_Solucion, btn3_3_Solucion, btn4_3_Solucion,
			btn1_4, btn2_4, btn3_4, btn4_4, btnComprovar4, btn1_4_Solucion, btn2_4_Solucion, btn3_4_Solucion,
			btn4_4_Solucion, btn1_5, btn2_5, btn3_5, btn4_5, btnComprovar5, btn1_5_Solucion, btn2_5_Solucion,
			btn3_5_Solucion, btn4_5_Solucion, btn1_6, btn2_6, btn3_6, btn4_6, btnComprovar6, btn1_6_Solucion,
			btn2_6_Solucion, btn3_6_Solucion, btn4_6_Solucion, btn1_7, btn2_7, btn3_7, btn4_7, btnComprovar7,
			btn1_7_Solucion, btn2_7_Solucion, btn3_7_Solucion, btn4_7_Solucion, btn1_8, btn2_8, btn3_8, btn4_8,
			btnComprovar8, btn1_8_Solucion, btn2_8_Solucion, btn3_8_Solucion, btn4_8_Solucion;

	// Ints color del boton
	public int btn1_color = 0;
	public int btn2_color = 0;
	public int btn3_color = 0;
	public int btn4_color = 0;

	// Ha ganado?
	public boolean victoria = false;
	public int intento = 0;
	public final int INTENTO_MAX = 8;

	public PanelMedio(Colores colores) {
		// PANEL
		contentPane = new JPanel();
		contentPane.setLayout(null);

		// Colores Solucion
		coloresDisponibles.add(colores.getC1());
		coloresDisponibles.add(colores.getC2());
		coloresDisponibles.add(colores.getC3());
		coloresDisponibles.add(colores.getC4());
		coloresDisponibles.add(colores.getC5());

		
		solucion1 = coloresDisponibles.get(r.nextInt(4));
		solucion2 = coloresDisponibles.get(r.nextInt(4));
		solucion3 = coloresDisponibles.get(r.nextInt(4));
		solucion4 = coloresDisponibles.get(r.nextInt(4));

		// Botones Colores Disponibles
		JLabel lblColoresDisponibles = new JLabel("Colores Disponibles");
		lblColoresDisponibles.setBounds(25, 30, 150, 14);
		contentPane.add(lblColoresDisponibles);

		JButton btn1_0 = new JButton("");
		btn1_0.setEnabled(false);
		btn1_0.setBackground(coloresDisponibles.get(0));
		btn1_0.setBounds(25, 50, 30, 30);
		contentPane.add(btn1_0);

		JButton btn2_0 = new JButton("");
		btn2_0.setEnabled(false);
		btn2_0.setBackground(coloresDisponibles.get(1));
		btn2_0.setBounds(65, 50, 30, 30);
		contentPane.add(btn2_0);

		JButton btn3_0 = new JButton("");
		btn3_0.setEnabled(false);
		btn3_0.setBackground(coloresDisponibles.get(2));
		btn3_0.setBounds(105, 50, 30, 30);
		contentPane.add(btn3_0);

		JButton btn4_0 = new JButton("");
		btn4_0.setEnabled(false);
		btn4_0.setBackground(coloresDisponibles.get(3));
		btn4_0.setBounds(145, 50, 30, 30);
		contentPane.add(btn4_0);

		JButton btn5_0 = new JButton("");
		btn5_0.setEnabled(false);
		btn5_0.setBackground(coloresDisponibles.get(4));
		btn5_0.setBounds(185, 50, 30, 30);
		contentPane.add(btn5_0);

		// Botones Combinacion Secreta
		JLabel lblCombinacionSecreta = new JLabel("Combinacion Secreta");
		lblCombinacionSecreta.setBounds(305, 30, 150, 14);
		contentPane.add(lblCombinacionSecreta);

		btnColorSecreto_4 = new JButton("");
		btnColorSecreto_4.setEnabled(false);
		btnColorSecreto_4.setVisible(true);
		btnColorSecreto_4.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_4.setBounds(425, 50, 30, 30);
		contentPane.add(btnColorSecreto_4);

		btnColorSecreto_3 = new JButton("");
		btnColorSecreto_3.setEnabled(false);
		btnColorSecreto_3.setVisible(true);
		btnColorSecreto_3.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_3.setBounds(385, 50, 30, 30);
		contentPane.add(btnColorSecreto_3);

		btnColorSecreto_2 = new JButton("");
		btnColorSecreto_2.setEnabled(false);
		btnColorSecreto_2.setVisible(true);
		btnColorSecreto_2.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_2.setBounds(345, 50, 30, 30);
		contentPane.add(btnColorSecreto_2);

		btnColorSecreto_1 = new JButton("");
		btnColorSecreto_1.setEnabled(false);
		btnColorSecreto_1.setVisible(true);
		btnColorSecreto_1.setBackground(Color.LIGHT_GRAY);
		btnColorSecreto_1.setBounds(305, 50, 30, 30);
		contentPane.add(btnColorSecreto_1);

		//btnColorSecreto_1.setBackground(solucion1);
		//btnColorSecreto_2.setBackground(solucion2);
		//btnColorSecreto_3.setBackground(solucion3);
		//btnColorSecreto_4.setBackground(solucion4);

		// Intento 8
		// Botones
		btn1_8 = new JButton("");
		btn1_8.setEnabled(false);
		btn1_8.setBackground(Color.WHITE);
		btn1_8.setBounds(25, 396, 30, 30);
		contentPane.add(btn1_8);

		btn2_8 = new JButton("");
		btn2_8.setEnabled(false);
		btn2_8.setBackground(Color.WHITE);
		btn2_8.setBounds(65, 396, 30, 30);
		contentPane.add(btn2_8);

		btn3_8 = new JButton("");
		btn3_8.setEnabled(false);
		btn3_8.setBackground(Color.WHITE);
		btn3_8.setBounds(105, 396, 30, 30);
		contentPane.add(btn3_8);

		btn4_8 = new JButton("");
		btn4_8.setEnabled(false);
		btn4_8.setBackground(Color.WHITE);
		btn4_8.setBounds(145, 396, 30, 30);
		contentPane.add(btn4_8);

		// Comprovacion
		btn4_8_Solucion = new JButton("");
		btn4_8_Solucion.setEnabled(false);
		btn4_8_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_8_Solucion.setBounds(425, 396, 30, 30);
		contentPane.add(btn4_8_Solucion);

		btn3_8_Solucion = new JButton("");
		btn3_8_Solucion.setEnabled(false);
		btn3_8_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_8_Solucion.setBounds(385, 396, 30, 30);
		contentPane.add(btn3_8_Solucion);

		btn2_8_Solucion = new JButton("");
		btn2_8_Solucion.setEnabled(false);
		btn2_8_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_8_Solucion.setBounds(345, 396, 30, 30);
		contentPane.add(btn2_8_Solucion);

		btn1_8_Solucion = new JButton("");
		btn1_8_Solucion.setEnabled(false);
		btn1_8_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_8_Solucion.setBounds(305, 396, 30, 30);
		contentPane.add(btn1_8_Solucion);

		// Boton Comprovar
		btnComprovar8 = new JButton("Comprobar");
		btnComprovar8.setEnabled(false);
		btnComprovar8.setBounds(185, 396, 110, 30);
		contentPane.add(btnComprovar8);

		// Intento 7
		// Botones
		btn1_7 = new JButton("");
		btn1_7.setEnabled(false);
		btn1_7.setBackground(Color.WHITE);
		btn1_7.setBounds(25, 355, 30, 30);
		contentPane.add(btn1_7);

		btn2_7 = new JButton("");
		btn2_7.setEnabled(false);
		btn2_7.setBackground(Color.WHITE);
		btn2_7.setBounds(65, 355, 30, 30);
		contentPane.add(btn2_7);

		btn3_7 = new JButton("");
		btn3_7.setEnabled(false);
		btn3_7.setBackground(Color.WHITE);
		btn3_7.setBounds(105, 355, 30, 30);
		contentPane.add(btn3_7);

		btn4_7 = new JButton("");
		btn4_7.setEnabled(false);
		btn4_7.setBackground(Color.WHITE);
		btn4_7.setBounds(145, 355, 30, 30);
		contentPane.add(btn4_7);

		// Comprovacion
		btn4_7_Solucion = new JButton("");
		btn4_7_Solucion.setEnabled(false);
		btn4_7_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_7_Solucion.setBounds(425, 355, 30, 30);
		contentPane.add(btn4_7_Solucion);

		btn3_7_Solucion = new JButton("");
		btn3_7_Solucion.setEnabled(false);
		btn3_7_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_7_Solucion.setBounds(385, 355, 30, 30);
		contentPane.add(btn3_7_Solucion);

		btn2_7_Solucion = new JButton("");
		btn2_7_Solucion.setEnabled(false);
		btn2_7_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_7_Solucion.setBounds(345, 355, 30, 30);
		contentPane.add(btn2_7_Solucion);

		btn1_7_Solucion = new JButton("");
		btn1_7_Solucion.setEnabled(false);
		btn1_7_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_7_Solucion.setBounds(305, 355, 30, 30);
		contentPane.add(btn1_7_Solucion);

		// Boton Comprovar
		btnComprovar7 = new JButton("Comprobar");
		btnComprovar7.setEnabled(false);
		btnComprovar7.setBounds(185, 355, 110, 30);
		contentPane.add(btnComprovar7);

		// Intento 6
		// Botones
		btn1_6 = new JButton("");
		btn1_6.setEnabled(false);
		btn1_6.setBackground(Color.WHITE);
		btn1_6.setBounds(25, 314, 30, 30);
		contentPane.add(btn1_6);

		btn2_6 = new JButton("");
		btn2_6.setEnabled(false);
		btn2_6.setBackground(Color.WHITE);
		btn2_6.setBounds(65, 314, 30, 30);
		contentPane.add(btn2_6);

		btn3_6 = new JButton("");
		btn3_6.setEnabled(false);
		btn3_6.setBackground(Color.WHITE);
		btn3_6.setBounds(105, 314, 30, 30);
		contentPane.add(btn3_6);

		btn4_6 = new JButton("");
		btn4_6.setEnabled(false);
		btn4_6.setBackground(Color.WHITE);
		btn4_6.setBounds(145, 314, 30, 30);
		contentPane.add(btn4_6);

		// Comprovacion
		btn4_6_Solucion = new JButton("");
		btn4_6_Solucion.setEnabled(false);
		btn4_6_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_6_Solucion.setBounds(425, 314, 30, 30);
		contentPane.add(btn4_6_Solucion);

		btn3_6_Solucion = new JButton("");
		btn3_6_Solucion.setEnabled(false);
		btn3_6_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_6_Solucion.setBounds(385, 314, 30, 30);
		contentPane.add(btn3_6_Solucion);

		btn2_6_Solucion = new JButton("");
		btn2_6_Solucion.setEnabled(false);
		btn2_6_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_6_Solucion.setBounds(345, 314, 30, 30);
		contentPane.add(btn2_6_Solucion);

		btn1_6_Solucion = new JButton("");
		btn1_6_Solucion.setEnabled(false);
		btn1_6_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_6_Solucion.setBounds(305, 314, 30, 30);
		contentPane.add(btn1_6_Solucion);

		// Boton Comprovar
		btnComprovar6 = new JButton("Comprobar");
		btnComprovar6.setEnabled(false);
		btnComprovar6.setBounds(185, 314, 110, 30);
		contentPane.add(btnComprovar6);

		// Intento 5
		// Botones
		btn1_5 = new JButton("");
		btn1_5.setEnabled(false);
		btn1_5.setBackground(Color.WHITE);
		btn1_5.setBounds(25, 273, 30, 30);
		contentPane.add(btn1_5);

		btn2_5 = new JButton("");
		btn2_5.setEnabled(false);
		btn2_5.setBackground(Color.WHITE);
		btn2_5.setBounds(65, 273, 30, 30);
		contentPane.add(btn2_5);

		btn3_5 = new JButton("");
		btn3_5.setEnabled(false);
		btn3_5.setBackground(Color.WHITE);
		btn3_5.setBounds(105, 273, 30, 30);
		contentPane.add(btn3_5);

		btn4_5 = new JButton("");
		btn4_5.setEnabled(false);
		btn4_5.setBackground(Color.WHITE);
		btn4_5.setBounds(145, 273, 30, 30);
		contentPane.add(btn4_5);

		// Comprovacion
		btn4_5_Solucion = new JButton("");
		btn4_5_Solucion.setEnabled(false);
		btn4_5_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_5_Solucion.setBounds(425, 273, 30, 30);
		contentPane.add(btn4_5_Solucion);

		btn3_5_Solucion = new JButton("");
		btn3_5_Solucion.setEnabled(false);
		btn3_5_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_5_Solucion.setBounds(385, 273, 30, 30);
		contentPane.add(btn3_5_Solucion);

		btn2_5_Solucion = new JButton("");
		btn2_5_Solucion.setEnabled(false);
		btn2_5_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_5_Solucion.setBounds(345, 273, 30, 30);
		contentPane.add(btn2_5_Solucion);

		btn1_5_Solucion = new JButton("");
		btn1_5_Solucion.setEnabled(false);
		btn1_5_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_5_Solucion.setBounds(305, 273, 30, 30);
		contentPane.add(btn1_5_Solucion);

		// Boton Comprovar
		btnComprovar5 = new JButton("Comprobar");
		btnComprovar5.setEnabled(false);
		btnComprovar5.setBounds(185, 273, 110, 30);
		contentPane.add(btnComprovar5);

		// Intento 4
		// Botones
		btn1_4 = new JButton("");
		btn1_4.setEnabled(false);
		btn1_4.setBackground(Color.WHITE);
		btn1_4.setBounds(25, 232, 30, 30);
		contentPane.add(btn1_4);

		btn2_4 = new JButton("");
		btn2_4.setEnabled(false);
		btn2_4.setBackground(Color.WHITE);
		btn2_4.setBounds(65, 232, 30, 30);
		contentPane.add(btn2_4);

		btn3_4 = new JButton("");
		btn3_4.setEnabled(false);
		btn3_4.setBackground(Color.WHITE);
		btn3_4.setBounds(105, 232, 30, 30);
		contentPane.add(btn3_4);

		btn4_4 = new JButton("");
		btn4_4.setEnabled(false);
		btn4_4.setBackground(Color.WHITE);
		btn4_4.setBounds(145, 232, 30, 30);
		contentPane.add(btn4_4);

		// Comprovacion
		btn4_4_Solucion = new JButton("");
		btn4_4_Solucion.setEnabled(false);
		btn4_4_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_4_Solucion.setBounds(425, 232, 30, 30);
		contentPane.add(btn4_4_Solucion);

		btn3_4_Solucion = new JButton("");
		btn3_4_Solucion.setEnabled(false);
		btn3_4_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_4_Solucion.setBounds(385, 232, 30, 30);
		contentPane.add(btn3_4_Solucion);

		btn2_4_Solucion = new JButton("");
		btn2_4_Solucion.setEnabled(false);
		btn2_4_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_4_Solucion.setBounds(345, 232, 30, 30);
		contentPane.add(btn2_4_Solucion);

		btn1_4_Solucion = new JButton("");
		btn1_4_Solucion.setEnabled(false);
		btn1_4_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_4_Solucion.setBounds(305, 232, 30, 30);
		contentPane.add(btn1_4_Solucion);

		// Boton Comprovar
		btnComprovar4 = new JButton("Comprobar");
		btnComprovar4.setEnabled(false);
		btnComprovar4.setBounds(185, 232, 110, 30);
		contentPane.add(btnComprovar4);

		// Intento 3
		// Botones
		btn1_3 = new JButton("");
		btn1_3.setEnabled(false);
		btn1_3.setBackground(Color.WHITE);
		btn1_3.setBounds(25, 191, 30, 30);
		contentPane.add(btn1_3);

		btn2_3 = new JButton("");
		btn2_3.setEnabled(false);
		btn2_3.setBackground(Color.WHITE);
		btn2_3.setBounds(65, 191, 30, 30);
		contentPane.add(btn2_3);

		btn3_3 = new JButton("");
		btn3_3.setEnabled(false);
		btn3_3.setBackground(Color.WHITE);
		btn3_3.setBounds(105, 191, 30, 30);
		contentPane.add(btn3_3);

		btn4_3 = new JButton("");
		btn4_3.setEnabled(false);
		btn4_3.setBackground(Color.WHITE);
		btn4_3.setBounds(145, 191, 30, 30);
		contentPane.add(btn4_3);

		// Comprovacion
		btn4_3_Solucion = new JButton("");
		btn4_3_Solucion.setEnabled(false);
		btn4_3_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_3_Solucion.setBounds(425, 191, 30, 30);
		contentPane.add(btn4_3_Solucion);

		btn3_3_Solucion = new JButton("");
		btn3_3_Solucion.setEnabled(false);
		btn3_3_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_3_Solucion.setBounds(385, 191, 30, 30);
		contentPane.add(btn3_3_Solucion);

		btn2_3_Solucion = new JButton("");
		btn2_3_Solucion.setEnabled(false);
		btn2_3_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_3_Solucion.setBounds(345, 191, 30, 30);
		contentPane.add(btn2_3_Solucion);

		btn1_3_Solucion = new JButton("");
		btn1_3_Solucion.setEnabled(false);
		btn1_3_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_3_Solucion.setBounds(305, 191, 30, 30);
		contentPane.add(btn1_3_Solucion);

		// Boton Comprovar
		btnComprovar3 = new JButton("Comprobar");
		btnComprovar3.setEnabled(false);
		btnComprovar3.setBounds(185, 191, 110, 30);
		contentPane.add(btnComprovar3);

		// Intento 2
		// Botones
		btn1_2 = new JButton("");
		btn1_2.setEnabled(false);
		btn1_2.setBackground(Color.WHITE);
		btn1_2.setBounds(25, 150, 30, 30);
		contentPane.add(btn1_2);

		btn2_2 = new JButton("");
		btn2_2.setEnabled(false);
		btn2_2.setBackground(Color.WHITE);
		btn2_2.setBounds(65, 150, 30, 30);
		contentPane.add(btn2_2);

		btn3_2 = new JButton("");
		btn3_2.setEnabled(false);
		btn3_2.setBackground(Color.WHITE);
		btn3_2.setBounds(105, 150, 30, 30);
		contentPane.add(btn3_2);

		btn4_2 = new JButton("");
		btn4_2.setEnabled(false);
		btn4_2.setBackground(Color.WHITE);
		btn4_2.setBounds(145, 150, 30, 30);
		contentPane.add(btn4_2);

		// Comprovacion
		btn4_2_Solucion = new JButton("");
		btn4_2_Solucion.setEnabled(false);
		btn4_2_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_2_Solucion.setBounds(425, 150, 30, 30);
		contentPane.add(btn4_2_Solucion);

		btn3_2_Solucion = new JButton("");
		btn3_2_Solucion.setEnabled(false);
		btn3_2_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_2_Solucion.setBounds(385, 150, 30, 30);
		contentPane.add(btn3_2_Solucion);

		btn2_2_Solucion = new JButton("");
		btn2_2_Solucion.setEnabled(false);
		btn2_2_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_2_Solucion.setBounds(345, 150, 30, 30);
		contentPane.add(btn2_2_Solucion);

		btn1_2_Solucion = new JButton("");
		btn1_2_Solucion.setEnabled(false);
		btn1_2_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_2_Solucion.setBounds(305, 150, 30, 30);
		contentPane.add(btn1_2_Solucion);

		// Boton Comprovar
		btnComprovar2 = new JButton("Comprobar");
		btnComprovar2.setEnabled(false);
		btnComprovar2.setBounds(185, 150, 110, 30);
		contentPane.add(btnComprovar2);

		// Intento 1
		// Botones
		btn1_1 = new JButton("");
		btn1_1.setBackground(Color.WHITE);
		btn1_1.setBounds(25, 110, 30, 30);
		contentPane.add(btn1_1);

		btn2_1 = new JButton("");
		btn2_1.setBackground(Color.WHITE);
		btn2_1.setBounds(65, 110, 30, 30);
		contentPane.add(btn2_1);

		btn3_1 = new JButton("");
		btn3_1.setBackground(Color.WHITE);
		btn3_1.setBounds(105, 110, 30, 30);
		contentPane.add(btn3_1);

		btn4_1 = new JButton("");
		btn4_1.setBackground(Color.WHITE);
		btn4_1.setBounds(145, 110, 30, 30);
		contentPane.add(btn4_1);

		// Comprovacion
		btn4_1_Solucion = new JButton("");
		btn4_1_Solucion.setEnabled(false);
		btn4_1_Solucion.setBackground(Color.LIGHT_GRAY);
		btn4_1_Solucion.setBounds(425, 110, 30, 30);
		contentPane.add(btn4_1_Solucion);

		btn3_1_Solucion = new JButton("");
		btn3_1_Solucion.setEnabled(false);
		btn3_1_Solucion.setBackground(Color.LIGHT_GRAY);
		btn3_1_Solucion.setBounds(385, 110, 30, 30);
		contentPane.add(btn3_1_Solucion);

		btn2_1_Solucion = new JButton("");
		btn2_1_Solucion.setEnabled(false);
		btn2_1_Solucion.setBackground(Color.LIGHT_GRAY);
		btn2_1_Solucion.setBounds(345, 110, 30, 30);
		contentPane.add(btn2_1_Solucion);

		btn1_1_Solucion = new JButton("");
		btn1_1_Solucion.setEnabled(false);
		btn1_1_Solucion.setBackground(Color.LIGHT_GRAY);
		btn1_1_Solucion.setBounds(305, 110, 30, 30);
		contentPane.add(btn1_1_Solucion);

		// Boton Comprovar
		btnComprovar1 = new JButton("Comprobar");
		btnComprovar1.setEnabled(true);
		btnComprovar1.setBounds(185, 110, 110, 30);
		contentPane.add(btnComprovar1);

		// Hacemos visible la ventana
		contentPane.setVisible(true);

		// Usamos el Metodo Test para asignar la funcion del juego a todos los botones.
		test(btn1_1, btn2_1, btn3_1, btn4_1, btnComprovar1, btn1_1_Solucion, btn2_1_Solucion, btn3_1_Solucion,
				btn4_1_Solucion, btnColorSecreto_1, btnColorSecreto_2, btnColorSecreto_3, btnColorSecreto_4, btn1_2,
				btn2_2, btn3_2, btn4_2, btnComprovar2);
		test(btn1_2, btn2_2, btn3_2, btn4_2, btnComprovar2, btn1_2_Solucion, btn2_2_Solucion, btn3_2_Solucion,
				btn4_2_Solucion, btnColorSecreto_1, btnColorSecreto_2, btnColorSecreto_3, btnColorSecreto_4, btn1_3,
				btn2_3, btn3_3, btn4_3, btnComprovar3);
		test(btn1_3, btn2_3, btn3_3, btn4_3, btnComprovar3, btn1_3_Solucion, btn2_3_Solucion, btn3_3_Solucion,
				btn4_3_Solucion, btnColorSecreto_1, btnColorSecreto_2, btnColorSecreto_3, btnColorSecreto_4, btn1_4,
				btn2_4, btn3_4, btn4_4, btnComprovar4);
		test(btn1_4, btn2_4, btn3_4, btn4_4, btnComprovar4, btn1_4_Solucion, btn2_4_Solucion, btn3_4_Solucion,
				btn4_4_Solucion, btnColorSecreto_1, btnColorSecreto_2, btnColorSecreto_3, btnColorSecreto_4, btn1_5,
				btn2_5, btn3_5, btn4_5, btnComprovar5);
		test(btn1_5, btn2_5, btn3_5, btn4_5, btnComprovar5, btn1_5_Solucion, btn2_5_Solucion, btn3_5_Solucion,
				btn4_5_Solucion, btnColorSecreto_1, btnColorSecreto_2, btnColorSecreto_3, btnColorSecreto_4, btn1_6,
				btn2_6, btn3_6, btn4_6, btnComprovar6);
		test(btn1_6, btn2_6, btn3_6, btn4_6, btnComprovar6, btn1_6_Solucion, btn2_6_Solucion, btn3_6_Solucion,
				btn4_6_Solucion, btnColorSecreto_1, btnColorSecreto_2, btnColorSecreto_3, btnColorSecreto_4, btn1_7,
				btn2_7, btn3_7, btn4_7, btnComprovar7);
		test(btn1_7, btn2_7, btn3_7, btn4_7, btnComprovar7, btn1_7_Solucion, btn2_7_Solucion, btn3_7_Solucion,
				btn4_7_Solucion, btnColorSecreto_1, btnColorSecreto_2, btnColorSecreto_3, btnColorSecreto_4, btn1_8,
				btn2_8, btn3_8, btn4_8, btnComprovar8);
		test(btn1_8, btn2_8, btn3_8, btn4_8, btnComprovar8, btn1_8_Solucion, btn2_8_Solucion, btn3_8_Solucion,
				btn4_8_Solucion, btnColorSecreto_1, btnColorSecreto_2, btnColorSecreto_3, btnColorSecreto_4, btn1_1,
				btn2_1, btn3_1, btn4_1, btnComprovar1);

	}

	public void test(JButton btn1_1, JButton btn2_1, JButton btn3_1, JButton btn4_1, JButton btnComprovar1,
			JButton btn1_1_Solucion, JButton btn2_1_Solucion, JButton btn3_1_Solucion, JButton btn4_1_Solucion,
			JButton btnColorSecreto_1, JButton btnColorSecreto_2, JButton btnColorSecreto_3, JButton btnColorSecreto_4,
			JButton btn1_2, JButton btn2_2, JButton btn3_2, JButton btn4_2, JButton btnComprovar2) {

		// Accion cambiar color de los btne 1 al 4
		btn1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn1_color > 4) {
					btn1_color = 0;
				}
				btn1_1.setBackground(coloresDisponibles.get(btn1_color));
				btn1_color++;
			}
		});

		btn2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn2_color > 4) {
					btn2_color = 0;
				}
				btn2_1.setBackground(coloresDisponibles.get(btn2_color));
				btn2_color++;
			}
		});

		btn3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn3_color > 4) {
					btn3_color = 0;
				}
				btn3_1.setBackground(coloresDisponibles.get(btn3_color));
				btn3_color++;
			}
		});

		btn4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn4_color > 4) {
					btn4_color = 0;
				}
				btn4_1.setBackground(coloresDisponibles.get(btn4_color));
				btn4_color++;
			}
		});

		// Comprovacion
		btnComprovar1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// +1 Intento
				intento++;

				// Inicializamos a 0 todos los colores de la ruleta.
				btn1_color = 0;
				btn2_color = 0;
				btn3_color = 0;
				btn4_color = 0;

				// Negras y blancas no puseden pasar de 4 entre las 2.
				int black = 0;
				int white = 0;

				// Metemos todos los colores y soluciones en un array para hacer las
				// comprobaciónes en un bucle.
				int max_i = 2;
				int max_j = 4;
				boolean aciertos[] = new boolean[4];
				Color colores[][] = new Color[max_i][max_j];

				colores[0][0] = btn1_1.getBackground();
				colores[0][1] = btn2_1.getBackground();
				colores[0][2] = btn3_1.getBackground();
				colores[0][3] = btn4_1.getBackground();
				colores[1][0] = solucion1;
				colores[1][1] = solucion2;
				colores[1][2] = solucion3;
				colores[1][3] = solucion4;

				// Comprovamos las negras
				for (int i = 0; i < 1; i++) {
					for (int j = 0; j < max_j; j++) {
						Color test = colores[i][j];
						Color sol = colores[1][j];
						if (test == sol) {
							aciertos[j] = true;
							black++;
						} else {
							aciertos[j] = false;
						}
					}
				}

				// Comprobamos las blancas
				for (int i = 0; i < 1; i++) {
					for (int j = 0; j < max_j; j++) {
						if (!aciertos[j]) {
							if (colores[0][j] == colores[1][0] || colores[0][j] == colores[1][1]
									|| colores[0][j] == colores[1][2] || colores[0][j] == colores[1][3]) {
								if (colores[0][j] == colores[1][0] && !aciertos[0]) {
									white++;
								} else if (colores[0][j] == colores[1][1] && !aciertos[1]) {
									white++;
								} else if (colores[0][j] == colores[1][2] && !aciertos[2]) {
									white++;
								} else if (colores[0][j] == colores[1][3] && !aciertos[3]) {
									white++;
								}

							}
						}
					}
				}

				if (MasterMindApp.debug) System.out.println("Casillas Negras " + black);
				if (MasterMindApp.debug) System.out.println("Casillas Blancas " + white);

				int botonesPintados = 0;
				int control1 = 0;
				int control2 = 0;

				while (black > control1) {
					if (botonesPintados == 0) {
						btn1_1_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_1_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_1_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_1_Solucion.setBackground(Color.BLACK);
						botonesPintados++;
					}
					control1++;
				}

				while (white > control2) {
					if (botonesPintados == 0) {
						btn1_1_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 1) {
						btn2_1_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 2) {
						btn3_1_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					} else if (botonesPintados == 3) {
						btn4_1_Solucion.setBackground(Color.WHITE);
						botonesPintados++;
					}
					control2++;
				}

				if (black == 4) { // Victoria!
					// Mostrar la combinacion
					btnColorSecreto_1.setBackground(solucion1);
					btnColorSecreto_2.setBackground(solucion2);
					btnColorSecreto_3.setBackground(solucion3);
					btnColorSecreto_4.setBackground(solucion4);

					// Desactivar los botones actuales
					btnComprovar1.setEnabled(false);
					btn4_1.setEnabled(false);
					btn3_1.setEnabled(false);
					btn2_1.setEnabled(false);
					btn1_1.setEnabled(false);

					// Mostrar mensaje de que has ganado
					JOptionPane.showMessageDialog(null, "¡Felicidades, has ganado!");
					victoria = true;

					// ActivarOpciones
					juegoController.setHaJugadoTrue();

				} else { // Derrota!
					// Desactivar los botones actuales
					btn4_1.setEnabled(false);
					btn3_1.setEnabled(false);
					btn2_1.setEnabled(false);
					btn1_1.setEnabled(false);
					btnComprovar1.setEnabled(false);

					if (intento < INTENTO_MAX) {
						// Activamos los botones
						btnComprovar2.setEnabled(true);
						btn4_2.setEnabled(true);
						btn3_2.setEnabled(true);
						btn2_2.setEnabled(true);
						btn1_2.setEnabled(true);
					} else if (intento >= INTENTO_MAX) {
						// Mostrar la combinacion
						btnColorSecreto_1.setBackground(solucion1);
						btnColorSecreto_2.setBackground(solucion2);
						btnColorSecreto_3.setBackground(solucion3);
						btnColorSecreto_4.setBackground(solucion4);
						JOptionPane.showMessageDialog(null, "¡Ohh noo! has perdido :( ");

						// ActivarOpciones
						juegoController.setHaJugadoTrue();
					}

				}
			}
		});
	}

	public void setCordinador(JuegoController juegoController) {
		this.juegoController = juegoController;
	}
}