package com.team3.MasterMind.view.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.team3.MasterMind.AppMain.MasterMindApp;
import com.team3.MasterMind.controller.JuegoController;
import java.awt.Color;
import javax.swing.JLabel;

public class VentanaOpciones extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	private JuegoController juegoController;
	private JButton btnAceptar, btnCancelar, btn6, btn5, btn4, btn3, btn2, btn1;
	
	public VentanaOpciones() {
		
		// Botones colores
		btn1 = new JButton("");
		btn1.setBackground(Color.WHITE);
		btn1.setBounds(10, 25, 30, 30);
		
		btn2 = new JButton("");
		btn2.setBackground(Color.WHITE);
		btn2.setBounds(50, 25, 30, 30);
		
		btn3 = new JButton("");
		btn3.setBackground(Color.WHITE);
		btn3.setBounds(90, 25, 30, 30);
		
		btn4 = new JButton("");
		btn4.setBackground(Color.WHITE);
		btn4.setBounds(130, 25, 30, 30);
		
		btn5 = new JButton("");
		btn5.setBackground(Color.WHITE);
		btn5.setBounds(170, 25, 30, 30);
		
		btn6 = new JButton("");
		btn6.setBackground(Color.WHITE);
		btn6.setBounds(210, 25, 30, 30);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(75, 80, 101, 23);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(75, 114, 101, 23);

		
		btn1.addActionListener(this);
		btn2.addActionListener(this);
		btn3.addActionListener(this);
		btn4.addActionListener(this);
		btn5.addActionListener(this);
		btn6.addActionListener(this);
		btnAceptar.addActionListener(this);
		btnCancelar.addActionListener(this);


		getContentPane().add(btnAceptar);
		getContentPane().add(btnCancelar);
		getContentPane().add(btn6);
		getContentPane().add(btn5);
		getContentPane().add(btn4);
		getContentPane().add(btn3);
		getContentPane().add(btn2);
		getContentPane().add(btn1);
		
		
		setTitle("Opciones");
		setSize(265, 200);
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnAceptar) {
			if (MasterMindApp.debug)
				System.out.println("Aceptar VentanaOpciones");
			// Cerramos la ventana
			
			// Guardar los colores
			juegoController.editColores(btn1.getBackground(), btn2.getBackground(), btn3.getBackground(), btn4.getBackground(), btn5.getBackground(), btn6.getBackground());
			JOptionPane.showMessageDialog(null, "Inicia una nueva partida para aplicar los nuevos colores.", "Advertencia", JOptionPane.WARNING_MESSAGE);
			this.dispose();
		}
		if (e.getSource() == btnCancelar) {
			if (MasterMindApp.debug)
				System.out.println("Cerrar VentanaOpciones");
			// Cerramos la ventana
			this.dispose();
		}
		if (e.getSource() == btn1) {
		    Color colorInicial = Color.WHITE;
		    Color nuevoColor = JColorChooser.showDialog(null, "Elige el color", colorInicial);
		    btn1.setBackground(nuevoColor);
		}
		if (e.getSource() == btn2) {
		    Color colorInicial = Color.WHITE;
		    Color nuevoColor = JColorChooser.showDialog(null, "Elige el color", colorInicial);
		    btn2.setBackground(nuevoColor);
		}
		if (e.getSource() == btn3) {
		    Color colorInicial = Color.WHITE;
		    Color nuevoColor = JColorChooser.showDialog(null, "Elige el color", colorInicial);
		    btn3.setBackground(nuevoColor);
		}
		if (e.getSource() == btn4) {
		    Color colorInicial = Color.WHITE;
		    Color nuevoColor = JColorChooser.showDialog(null, "Elige el color", colorInicial);
		    btn4.setBackground(nuevoColor);
		}
		if (e.getSource() == btn5) {
		    Color colorInicial = Color.WHITE;
		    Color nuevoColor = JColorChooser.showDialog(null, "Elige el color", colorInicial);
		    btn5.setBackground(nuevoColor);
		}
		if (e.getSource() == btn6) {
		    Color colorInicial = Color.WHITE;
		    Color nuevoColor = JColorChooser.showDialog(null, "Elige el color", colorInicial);
		    btn6.setBackground(nuevoColor);
		}

	}


	public void setCordinador(JuegoController juegoController) {
		this.juegoController = juegoController;
	}
}
