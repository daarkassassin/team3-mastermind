package com.team3.MasterMind.view.JFrame;

import javax.swing.*;

import com.team3.MasterMind.AppMain.MasterMindApp;
import com.team3.MasterMind.controller.JuegoController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaNivel extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JuegoController juegoController;

	private JButton btnAceptar, btnCancelar;
	private ButtonGroup bgroup;
	private JRadioButton rdbtnPrincipiante, rdbtnMedio, rdbtnAvanzado;

	public VentanaNivel() {
		// Rdbtn Principiante / Medio / Avanzado
		rdbtnPrincipiante = new JRadioButton("Principiante");
		rdbtnPrincipiante.setSelected(true);
		rdbtnPrincipiante.setActionCommand("0");
		rdbtnPrincipiante.setBounds(123, 51, 109, 23);

		rdbtnMedio = new JRadioButton("Medio");
		rdbtnMedio.setActionCommand("1");
		rdbtnMedio.setBounds(123, 77, 109, 23);

		rdbtnAvanzado = new JRadioButton("Avanzado");
		rdbtnAvanzado.setActionCommand("2");
		rdbtnAvanzado.setBounds(123, 103, 109, 23);

		// JButton Aceptar / Cancelar
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(65, 209, 122, 43);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(197, 209, 122, 43);

		btnAceptar.addActionListener(this);
		btnCancelar.addActionListener(this);

		getContentPane().add(rdbtnPrincipiante);
		getContentPane().add(rdbtnMedio);
		getContentPane().add(rdbtnAvanzado);
		getContentPane().add(btnAceptar);
		getContentPane().add(btnCancelar);

		bgroup = new ButtonGroup();
		bgroup.add(rdbtnPrincipiante);
		bgroup.add(rdbtnMedio);
		bgroup.add(rdbtnAvanzado);

		setTitle("Elige la dificultad");
		setSize(400, 300);
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnAceptar) {
			if (MasterMindApp.debug)
				System.out.println("Aceptar VentanaNivel");
			// Obtenemos la elección del usuario
			String s_opc = bgroup.getSelection().getActionCommand();

			// Añadimos el nuevo panel segun la eleción del usuario
			if (s_opc.equals("0")) {
				juegoController.setPrincipiante();
			} else if (s_opc.equals("1")) {
				juegoController.setMedio();

			} else if (s_opc.equals("2")) {
				juegoController.setAvanzado();
			}

			this.dispose();
		}
		if (e.getSource() == btnCancelar) {
			if (MasterMindApp.debug)
				System.out.println("Cerrar VentanaNivel");
			// Cerramos la ventana
			this.dispose();
		}

	}

	public void setCordinador(JuegoController juegoController) {
		this.juegoController = juegoController;
	}

}
