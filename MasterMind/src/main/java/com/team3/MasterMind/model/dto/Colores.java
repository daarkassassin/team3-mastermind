package com.team3.MasterMind.model.dto;

import java.awt.Color;
import java.util.ArrayList;

public class Colores {

	private Color c1;
	private Color c2;
	private Color c3;
	private Color c4;
	private Color c5;
	private Color c6;

	public Colores() {
		this.c1 = new Color(0, 0, 0); // Negro
		this.c2 = new Color(255, 255, 0); // Amarillo
		this.c3 = new Color(102, 0, 153); // Purple
		this.c4 = new Color(0, 255, 0); // Verde
		this.c5 = new Color(255, 0, 0); // Rojo
		this.c6 = new Color(0, 0, 255); // Azul

	}

	public Colores(Color c1, Color c2, Color c3, Color c4, Color c5, Color c6) {
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.c4 = c4;
		this.c5 = c5;
		this.c6 = c6;
	}

	public Color getC1() {
		return c1;
	}

	public void setC1(Color c1) {
		this.c1 = c1;
	}

	public Color getC2() {
		return c2;
	}

	public void setC2(Color c2) {
		this.c2 = c2;
	}

	public Color getC3() {
		return c3;
	}

	public void setC3(Color c3) {
		this.c3 = c3;
	}

	public Color getC4() {
		return c4;
	}

	public void setC4(Color c4) {
		this.c4 = c4;
	}

	public Color getC5() {
		return c5;
	}

	public void setC5(Color c5) {
		this.c5 = c5;
	}

	public Color getC6() {
		return c6;
	}

	public void setC6(Color c6) {
		this.c6 = c6;
	}

}
